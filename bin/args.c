// Copyright CC-BY-SA-4.0 2017- Haelwenn (lanodan) Monnier <contact@hacktivis.me>

#define _POSIX_C_SOURCE 200809L
// printf()
#include <stdio.h>

int main(int argc, char *argv[])
{
	printf("argc: %i\n", argc);

	for(int i = 0; i < argc; i++)
	{
		printf("argv[%d]: \"%s\"\n", i, argv[i]);
	}

	return 0;
}
