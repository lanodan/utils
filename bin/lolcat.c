// Copyright 2018 Haelwenn (lanodan) Monnier <contact@hacktivis.me>
// Distributed under the terms of the CC-BY-SA-4.0 license

#define _POSIX_C_SOURCE 200809L
#include <stdio.h> /* getchar(), putchar(), snprintf(), printf() */
#include <math.h> /* sin() */

void rainbow(double freq, int i) {
	int red, green, blue;
	double pi = 3.14159;

	red = sin(freq*i + 0) * 127 + 128;
	green = sin(freq*i + 2*pi/3) * 127 + 128;
	blue = sin(freq*i + 4*pi/3) * 127 + 128;

	printf("[38;2;%02d;%02d;%02dm", red, green, blue);
	// TODO: Replace to sprintf?
}

int main(void) {
	int c, i;
	double freq = 0.1;

	i = 0;

	while((c = getchar()) != EOF)
	{
		rainbow(freq, i);
		i++;
		putchar(c);
	}

	printf("[0m");

	return 0;
}
