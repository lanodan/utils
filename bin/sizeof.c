#define _POSIX_C_SOURCE 200809L
#if __OpenBSD__
#include <stdlib.h>
#else
#include <bsd/stdlib.h>
#endif

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

static const char *format   = "sizeof(%s) == %d bytes; %d bits\n";
static const char *format_m = "sizeof(%s) == %d bytes; %d bits; (MIN:MAX) == (%d:%d)\n";

static void print_size(size_t size, char *type)
{
	int c;
	c = size;
	printf(format, type, c, c * CHAR_BIT);
}

int main(void)
{
	int c;

	printf("CHAR_BIT == %d\n", CHAR_BIT);

	c = sizeof(int);
	printf(format_m, "int", c, c * CHAR_BIT, INT_MIN, INT_MAX);
	c = sizeof(char);
	printf(format_m, "char", c, c * CHAR_BIT, CHAR_MIN, CHAR_MAX);

	print_size(sizeof(uint8_t), "uint8_t");
	print_size(sizeof(short), "short");
	print_size(sizeof(double), "double");
	print_size(sizeof(long), "long");
	print_size(sizeof(double long), "double long");
	print_size(sizeof(long long), "long long");
	print_size(sizeof(long int), "long int");
	print_size(sizeof(char[BUFSIZ]), "char[BUFSIZ]");
	print_size(sizeof(char[256]), "char[256]");
	print_size(sizeof(char[32]), "char[32]");
	print_size(sizeof(char[2]), "char[2]");
	print_size(sizeof('a'), "'a'");
	print_size(sizeof(arc4random()), "arc4random()");

	return 0;
}
