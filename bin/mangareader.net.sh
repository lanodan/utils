#!/bin/sh

name="${1}"

for chap in $(seq 1 "${2:-1}")
	do mkdir -p "${HOME}/${name}/${chap}"
	cd "$HOME/${name}/${chap}" || exit
	chap_pages=$(curl "http://www.mangareader.net/${name}/${chap}/" 2>/dev/null | grep '</select> of' | sed -r 's|</select> of ([0-9]*).*|\1|')
	curl "http://www.mangareader.net/${name}/${chap}/" 2>/dev/null | grep 'img id="img"' | sed -r 's|.* src="(.*)" alt.*|\1|' | xargs wget -c -O 1.jpg
	for p in $(seq 2 "${chap_pages}")
		do curl "http://www.mangareader.net/${name}/${chap}/${p}" 2>/dev/null | grep 'img id="img"' | sed -r 's|.* src="(.*)" alt.*|\1|' | xargs wget -c -O "${p}.jpg"
	done
done
