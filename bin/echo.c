// Copyright 2020 Haelwenn (lanodan) Monnier <contact@hacktivis.me>
// Distributed under the terms of the CC-BY-SA-4.0 license

#define _POSIX_C_SOURCE 200809L
#include <stdio.h> /* printf(), putchar() */

// Note: maybe consider buffering to write(2) only once like in Plan9
// https://web.archive.org/web/20150519100236/https://gist.github.com/dchest/1091803#comment-1398629
int
main(int argc, char *argv[])
{
	int i;

	for(i = 1; i < argc; i++)
		printf("%s ", argv[i]);

	putchar('\n');

	return 0;
}
