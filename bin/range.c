// Copyright CC-BY-SA-4.0 2017- Haelwenn (lanodan) Monnier <contact@hacktivis.me>

#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>

char *IFS;
int a, b;

void usage(char *a0) { printf("usage: %s [ini <max>]\n", a0); }

void range(int initial, int maximum)
{
	printf("%i", initial);
	if(initial < maximum)
	{
		for(initial++; initial <= maximum; initial++)
			printf("%s%i", IFS, initial);
	}
	else if(initial > maximum)
	{
		for(initial--; initial >= maximum; initial--)
			printf("%s%i", IFS, initial);
	}
}

int main(int argc, char *argv[])
{
	if(!(IFS = getenv("IFS")))
	{
		IFS = " ";
	}

	if(argc == 2)
	{
		range(0, atoi(argv[1]));
	}
	else if(argc == 3)
	{
		range(atoi(argv[1]), atoi(argv[2]));
	}
	else
	{
		usage(argv[0]);
	}
}
