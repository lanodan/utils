#!/bin/sh
export PS4='[32m>> [0m' ; set -x
emerge -q --sync
for i in "@security" "@world" "@preserved-rebuild"; do emerge -1uvDNq --with-bdeps=y --keep-going --verbose-conflicts $i; done
emerge -catv
eclean -d distfiles
eclean packages
