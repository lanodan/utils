#!/bin/sh
# Copyright CC-BY-SA-4.0 2017-2020 Haelwenn (lanodan) Monnier <contact@hacktivis.me>
xdd="${XDG_DATA_DIR:-$HOME/.local/share}/timey-whyme"
today="$(date --date=now +%Y-%m-%d)"
later="$(date --date=+7days +%Y-%m-%d)"

usage() {
	echo "$0"' [t…|c…|h…]
	c…: show calendar
	t…: show todo
	h…: this help'

	echo '
Files used:
	• '"$xdd"'/calendar.csv
	• '"$xdd"'/todo.csv'

	echo '
Environment used:
	XDG_DATA_DIR (defaults to “~/.local/share”)'
}

die() {
	echo "$1"
	exit
}

verify_xxd() {
	[ -e "${xdd}/${1}" ] || die "Please create the following file, see examples for help: ${xdd}/${1}"
}

todo_gen() {
# (GNU-ism) nowarn: turn off warning: table wider than line width
	echo \
'.color 1
.TS
nowarn;
llll.
_
.TH
Deadline	Nice	Description	Location
.TB
_'
	(
		cut -d '#' -f -1 "$xdd/todo.csv" | grep -v '^$'
		echo "${today},\m[blue],Now"
		echo "${later},,In 7 days\m[]"
	) | sort -n | tr ',' '	'
	echo '.TE'
}

calendar_gen() {
# (GNU-ism) nowarn: turn off warning: table wider than line width
	echo \
'.color 1
.TS
nowarn;
llll.
_
.TH
Start	End	Description	Location
.TB
_'
	(
		cut -d '#' -f -1 "$xdd/calendar.csv" | grep -v '^$'
		echo "${today},\m[blue],Now"
		echo "${later},,In 7 days\m[]"
	) | sort -n | tr ',' '	'
	echo '.TE'
}

calendar() {
	verify_xxd calendar.csv
	# -t : preprocess with tbl(1)
	# grep -v '^$' : remove empty lines
	calendar_gen | groff -t -Dutf-8 -Tutf8 | grep -v '^$'
}
todo() {
	verify_xxd todo.csv
	todo_gen | groff -t -Dutf-8 -Tutf8 | grep -v '^$'
}

case "$1" in
	c*)
		calendar
	;;
	t*)
		todo
	;;
	h*)
		usage
	;;
	*)
		todo
		calendar
	;;
esac
