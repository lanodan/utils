// Copyright 2018-2020 Haelwenn (lanodan) Monnier <contact@hacktivis.me>
// Distributed under the terms of the CC-BY-SA-4.0 license

#define _POSIX_C_SOURCE 200809L
#include <stdio.h> /* printf(), fread() */
#include <math.h> /* sin() */
#include <stdint.h> /* uint8_t */
#include <string.h> /* memset() */
#include <ctype.h> /* isprint() */

#define LANODAN_XCD_RESET printf("[0m[48;2;0;0;0m");
#define LANODAN_XCD_PRINT printf(" >%s<", line);

void rainbow(double freq, char i) {
	if(i == 0) {
		printf("[38;2;64;64;64m");
	} else {
		uint8_t red, green, blue;
		double pi = 3.14159;

		red = sin(freq*i + 0) * 127 + 128;
		green = sin(freq*i + 2*pi/3) * 127 + 128;
		blue = sin(freq*i + 4*pi/3) * 127 + 128;

		printf("[38;2;%02d;%02d;%02dm", red, green, blue);
	}
}

int main(void) {
	int cols = 0, bytes = 0;
	int line_width = 16;
	double freq = 0.2;
	char c, line[line_width];

	memset(&line, 0, line_width);

	LANODAN_XCD_RESET

	rainbow(freq, bytes);
	printf("%06x ", bytes);
	while(fread(&c, 1, 1, stdin) > 0)
	{
		if(cols >= line_width) {
			cols = 0;

			LANODAN_XCD_RESET
			LANODAN_XCD_PRINT
			memset(&line, 0, line_width);

			rainbow(freq, bytes);
			printf("\n%06x ", bytes);
		}

		rainbow(freq, c);
		printf("%02hhx ", c);
		line[cols] = isprint(c) ? c : '.';

		cols++;
		bytes++;
	}


	// Fill the rest of the hex space with spaces
	for(cols; cols < line_width; cols++) printf("   ");

	LANODAN_XCD_RESET
	LANODAN_XCD_PRINT

	rainbow(freq, bytes);
	printf("\n%06x\n", bytes);
	LANODAN_XCD_RESET

	return 0;
}
