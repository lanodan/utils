#!/bin/sh

name="${1}"

for chap in $(seq -w 001 "${2:-1}")
	do mkdir -p "${HOME}/${name}/${chap}"
	cd "${HOME}/${name}/${chap}" || exit
	total_pages=$(curl "http://www.mangahere.co/manga/${name}/c${chap}/" 2>/dev/null | grep 'total_pages' | sed -r 's|.*=.([0-9]*).*|\1|')
	curl "http://www.mangahere.co/manga/${name}/c${chap}/" 2>/dev/null | grep 'id="image"' | sed -r 's|.* src="([^"]*)" .*|\1|' | xargs wget -c -O 1.jpg
	for page in $(seq 2 "${total_pages}")
		do curl "http://www.mangahere.co/manga/${name}/c${chap}/${page}.html" 2>/dev/null | grep 'id="image"' | sed -r 's|.* src="([^"]*)" .*|\1|' | xargs wget -c -O "${page}.jpg"
	done
done
