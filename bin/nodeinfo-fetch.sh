#!/bin/sh
url="$1"
accept_header='Accept: application/json'
nodeinfo_url=''

die() {
	echo $@
	exit 1
}

if [[ -z "${url}" ]]
then
	die "Usage: ${0} [instance_url]"
fi

nodeinfo_url=$(curl -sSL -H "${accept_header}" "${url}/.well-known/nodeinfo" | jq '.links[].href' -r | sort | tail -1)

if [[ -z "${nodeinfo_url}" ]]
then
	die "Failed getting Nodeinfo endpoint"
fi

curl -sSL -H "${accept_header}" ${nodeinfo_url} | jq .
