// Copyright 2018 Haelwenn (lanodan) Monnier <contact@hacktivis.me>
// Distributed under the terms of the CC-BY-SA-4.0 license

#define _POSIX_C_SOURCE 200809L
#include <errno.h>  /* errno */
#include <stdio.h>  /* puts, perror, printf */
#include <stdlib.h> /* exit */
#include <string.h> /* strerror */
#include <unistd.h> /* getcwd() */

char buf[BUFSIZ];

void usage(char *a0)
{
	(void)printf("usage: %s\n", a0);
	exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[])
{
	if(argc != 1) usage(argv[0]);

	if(getcwd(buf, sizeof(buf)) == NULL)
	{
		perror(strerror(errno));
		exit(EXIT_FAILURE);
	}
	else
	{
		const char *pwd = buf;
		return puts(pwd);
	}
}
