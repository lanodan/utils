/* Copyright CC-BY-SA-4.0 2017-2020 Haelwenn (lanodan) Monnier <contact@hacktivis.me> */
#define _POSIX_C_SOURCE 200809L
#include <locale.h> /* setlocale() */
#include <stdio.h>  /* BUFSIZ, perror(), puts() */
#include <stdlib.h> /* exit() */
#include <time.h>   /* time, localtime, tm, strftime */
#include <unistd.h> /* getopt(), optarg, optind */

int main(int argc, char *argv[])
{
	char outstr[BUFSIZ];
	struct tm *tm;
	time_t now = time(NULL);
	char *format  = "%c";
	int uflag = 0;
	int c;

	setlocale(LC_ALL, "");

	while ((c = getopt(argc, argv, ":u")) != -1) {
		switch(c) {
		case 'u': /* UTC timezone */
			uflag++;
			break;
		}
	}

	if(uflag) {
		tm = gmtime(&now);

		if(tm == NULL)
		{
			perror("gmtime");
			exit(EXIT_FAILURE);
		}
	} else {
		tm = localtime(&now);

		if(tm == NULL)
		{
			perror("localtime");
			exit(EXIT_FAILURE);
		}
	}

	argc -= optind;
	argv += optind;

	if (*argv && **argv == '+')
		format = *argv + 1;

	if(strftime(outstr, sizeof(outstr), format, tm) == 0)
	{
		perror("strftime");
		exit(EXIT_FAILURE);
	}

	return puts(outstr);
}
