#!/bin/sh
acct="$1"
accept_header='Accept: application/json,application/xml'

die() {
	echo $@
	exit 1
}

if [[ -z "${acct}" ]]
then
	die "Usage: ${0} [webfinger acct]"
fi

host=$(cut -f2 -d'@' <<<"${acct}")

curl -sSL -H "${accept_header}" "http://${host}/.well-known/webfinger?resource=acct:${acct}"
