all:
	cd bin ; $(MAKE)

clean:
	cd bin ; $(MAKE) clean

install:
	cd bin ; $(MAKE) install
